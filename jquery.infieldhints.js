/******************************************************************************

infieldHints() jquery plugin written by Todd Resudek for Supersimple.org
Released November 2011

http://supersimple.org/infieldhints

Design based on work by Thibaut Ninove <http://wooconcept.com/>
Original design: http://dribbble.com/shots/327302-Infield-hints
Follow Thibaut: @tbnv

This function will add a hint in the input field.

Usage: This is a good way to add hints to fields for users without having a separate
label element.

The value is set by using the placeholder attribute. For any elements that you
want to have a hint value, add the placeholder attribute, like this:
<input type="text" title="Enter Your Website" placeholder="http://" value="" />

If you want dont want a hint, leave out the placeholder attribute.

The script creates an HTML element with the hint in it. That element will inherit
any classes applied directly to the input element.
For example, the input element:
<input type="text" title="Sample" placeholder="Hint" value="" class="blue txt" />
will create the hint:
<span class="infield-hint blue txt">Hint</span>

Common Issues:
Since the script modifies the css of the input element, the css for that input
must be applied at the time the javascript is called.
If the hint ends up being wider than the input it belongs to, the script will
act erratically.

==============================================================================

License: This script is released under the CC Attribution 3.0 license.
http://creativecommons.org/licenses/by/3.0/us/

You are free to share,copy,distribute,display and remix this script.

You must leave this comment intact. (fair is fair)
	
For any reuse or distribution, you must make clear to others the license terms 
of this work. The best way to do this is with a link to the CC web page.

Any of the above conditions can be waived if you get permission from the
copyright holder.

Apart from the remix rights granted under this license, nothing in this 
license impairs or restricts the author's moral rights.

******************************************************************************/

jQuery.fn.infieldhints = function() {
	
	$('input[placeholder!=""]').each(function(){
		if($(this).attr('placeholder')){ //skip any elements with no placeholder set
			$(this).wrap('<div class="input" />');
			$(this).after('<span class="infield-hint '+$(this).attr('class')+'">'+$(this).attr('placeholder')+'</span>');
			$(this).attr('placeholder','');
			$(this).css('padding-left',$(this).next().outerWidth()+parseInt($(this).next().css('left')));
			$(this).css('width',parseInt($(this).css('width'))-$(this).next().outerWidth()+parseInt($(this).next().css('left')));
		}
	});

}